/* Copyright (c) 2007-2017 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package minesweeper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import minesweeper.BoardTile.State;

/**
 * Minesweeper board of size MxN, such that every tile x is one of the following states:
 * - untouched 
 * - flagged
 * - dug
 * Additionally, untouched tiles may contain bombs.
 * The board is solved when every tile x that has a bomb is in state flagged
 * The game is played using traditional minesweeper rules with some additions to fit
 * multiplayer game.
 * When a player digs a tile that has a bomb, the tile is set to be dug
 */
public class Board {
    // Abstraction function:
    // Represent a board of m rows and n columns
    // This datatype is NOT threadsafe, please access only from one thread
    
    //rep invariant: m, n > 0
    
    //rep exposure: all fields are private final variables
    private final BoardTile[][] tiles;
    private final int m, n;
    
    public Board(int m, int n) {
        this.m = m;
        this.n = n;
        this.tiles = new BoardTile[m][n];
        initTiles();
    }
    
    /**
     * Init this board from given minefield
     * set board's tile i, j to have bomb iff minefield[i][j] is true
     * @param minefield
     */
    public Board(boolean[][] minefield) {
        if (minefield.length == 0 || minefield[0].length == 0) {
            throw new IllegalArgumentException("Invalid minefield length");
        }
        this.m = minefield.length;
        this.n = minefield[0].length;
        this.tiles = new BoardTile[m][n];
        initTiles(minefield);
    }
    
    /**
     * Initialize all tiles in this board to be untouched
     */
    private void initTiles() {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                tiles[i][j] = new BoardTile(i, j, false);
            }
        }
    }
    
    /**
     * Initialize all tiles in this board to be untouched,
     * unless for a tile i, j minefield[i][j] is true
     */
    private void initTiles(boolean[][] minefield) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                tiles[i][j] = new BoardTile(i, j, minefield[i][j]);
            }
        }
    }
    
    private boolean checkInBounds(int i, int j) {
        return (i >= 0 && j >= 0 && i < m && j < n);
    }
    
    public int getWidth() {
        return n;
    }
    
    public int getHeight() {
        return m;
    }
    
    /**
     * Dig tile located at x, y. If indices are out of bounds or the tile is not
     * in the untouched state then do nothing and return false
     * else set it to DUG, and
     *  -if there is a bomb then remove the bomb and return true
     *  -reveal this tile (and possibly more tiles if there are no
     *  mines neighboring this tile) and return false
     * @param x
     * @param y
     * @return true if the tile that has been dug contained a bomb
     */
    public boolean dig(int i, int j) {
        if (!checkInBounds(i, j)) {
            return false;
        }
        BoardTile tile = tiles[i][j];
        boolean exploded = false;
        if (tile.getState() == State.UNTOUCHED) {
            exploded = tile.hasBomb();
            if (exploded) {
                tile.setBomb(false);
            }
            reveal(tile);
        }
        return exploded;
    }
    
    /**
     * Set this tile to dug, and if there are no mined neighbors,
     * recursively reveal every neighbor
     * If tile is not in untouched state or has a bomb then do nothing
     * @param tile
     */
    private void reveal(BoardTile tile) {

        if (tile.getState() != State.UNTOUCHED || tile.hasBomb()) {
            return;
        }
        tile.setState(State.DUG);
        
        List<BoardTile> neighbors = getNeighbors(tile);
        // find first occurrence of a neighbor with a bomb
        Optional<BoardTile> minedNeighbor
            = neighbors.stream()
                .filter(t -> t.hasBomb())
                .findFirst();
        if (!minedNeighbor.isPresent()) {
            for (BoardTile neighbor: getNeighbors(tile)) {
                reveal(neighbor);
            }
        }
    }
    
    /**
     * Flag tile located at x, y if it's not in flagged state yet.
     * Otherwise do nothing
     * If indices are out of bounds then do nothing
     * @param x
     * @param y
     */
    public void flag(int i, int j) {
        if (!checkInBounds(i, j)) {
            return;
        }
        if (tiles[i][j].getState() == State.UNTOUCHED) {
            tiles[i][j].setState(State.FLAGGED);
        }
    }
    
    /**
     * Deflag tile located at x, y if it's in flagged state and make it untouched.
     * Otherwise do nothing
     * If indices are out of bounds then do nothing
     * @param x
     * @param y
     */
    public void deflag(int i, int j) {
        if (!checkInBounds(i, j)) {
            return;
        }
        if (tiles[i][j].getState() == State.FLAGGED) {
            tiles[i][j].setState(State.UNTOUCHED);
        }
    }
    
    /**
     * Return string representation of this board, with n rows separated by newlines
     * Each row of m characters, separated by spaces. The character depends on tile's state:
     * - "-" for tiles with state untouched
     * - "F" for tiles that are flagged
     * - " " (space) for tiles that are dug and 0 neighbors that have a bomb
     * - N (integer in range 1-8) for tiles with state dug and N amount of neighbors that have a bomb
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                switch (tiles[i][j].getState()) {
                    case FLAGGED:
                        builder.append("F");
                        break;
                    case UNTOUCHED:
                        builder.append("-");
                        break;
                    case DUG:
                        long bombsAround = getNeighborMinesNumber(tiles[i][j]);
                        if (bombsAround == 0) {
                            builder.append(" ");
                        }
                        else {
                            builder.append(Long.toString(bombsAround));
                        }
                        break;
                }
                if (j != n-1)
                    builder.append(" ");
            }
            if (i != m-1)
                builder.append("\n");
        }
        return builder.toString();
    }
    
    private List<BoardTile> getNeighbors(BoardTile tile) {
        int i = tile.getI();
        int j = tile.getJ();
        List<Pos> positions =
                Arrays.asList(new Pos[] {
                        new Pos(i-1, j-1),
                        new Pos(i-1, j),
                        new Pos(i-1, j+1),
                        new Pos(i, j-1),
                        new Pos(i, j+1),
                        new Pos(i+1, j-1),
                        new Pos(i+1, j),
                        new Pos(i+1, j+1)
                });
        return
            positions.stream()
                .filter(p -> checkInBounds(p.i, p.j))
                .map(p -> tiles[p.i][p.j])
                .collect(Collectors.toList());
    }
    
    /**
     * Get number of tiles that have a mine and that neighbor
     * given tile
     * @param tile
     * @return number of neighbors with mines
     */
    private long getNeighborMinesNumber(BoardTile tile) {
        return getNeighbors(tile).stream()
                .filter(n -> n.hasBomb())
                .collect(Collectors.counting());
    }

    // basically, a structure to represent a pair of ints
    private static class Pos {
        
        public final int i, j;
        public Pos(int i, int j) {
            this.i = i;
            this.j = j;
        }
    }
    
    /**
     * read board from a text file where first line is
     * M N\n that specify board size and next M lines each consisting of
     * N space separated symbols. Each symbol may be 0 or 1, denoting the absence
     * or presence of a bomb at the tile at
     * @param file
     * @return
     */
    public static Board readFromFile(File file) throws FileNotFoundException, IOException {
        try (BufferedReader reader = 
                new BufferedReader(new FileReader(file))) {
            String header = reader.readLine();
            String[] size = header.split("\\s");
            int rowNum, colNum;
            if (size.length != 2) {
                throw new RuntimeException("Wrong file header format: error in number of sizes");
            }
            try {
                colNum = Integer.parseInt(size[0]);
                rowNum = Integer.parseInt(size[1]);
            }
            catch (NumberFormatException ex) {
                throw new RuntimeException("Wrong file header format: error parsing size");
            }
            boolean[][] minefield = new boolean[rowNum][colNum];
            int currRow = 0;
            String line;
            while ((line = reader.readLine()) != null) {
                String[] row = line.split("\\s");
                if (row.length != colNum) {
                    throw new RuntimeException(String.format("Wrong line format (row number %d) "
                            + "number of elements: %d, should be %d", currRow, row.length, colNum));
                }
                for (int currCol = 0; currCol < colNum; currCol++) {
                    if (row[currCol].equals("0")) {
                        minefield[currRow][currCol] = false;
                    }
                    else if (row[currCol].equals("1")) {
                        minefield[currRow][currCol] = true;
                    }
                    else {
                        throw new RuntimeException(
                                String.format("Invalid symbol %s at %d, %d", row[currCol], currRow, currCol));
                    }
                }
                currRow++;
            }
            return new Board(minefield);
        }
    }
    
    /**
     * Create a randomly filled board of m rows and n columns
     * each tile in the board has specified probability to contain a mine
     * assume m, n > 0;
     * @param m
     * @param n
     * @param probability
     * @return
     */
    public static Board makeRandomBoard(int m, int n, double probability) {
        boolean[][] mines = new boolean[m][n];
        Random random = new Random();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                mines[i][j] = random.nextDouble() > probability;
            }
        }
        return new Board(mines);
    }
}
