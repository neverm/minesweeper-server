package minesweeper;

/**
 * One tile of a minesweeper board can be in dug, flagged or untouched states
 * as well as have a bomb
 * @author nvm
 *
 */
public class BoardTile {
    
    public enum State {DUG, FLAGGED, UNTOUCHED}
    
    private State state;
    private boolean hasBomb;
    private final int i, j;
    
    // abstraction function: 
    // represent a tile located at i, j in given state and possibility of having a bomb
    // where i is Y-coordinate and j is X-coordinate and origin is left-top corner of a board
    // rep invariant:
    // if state is DUG then hasBomb is false
    // rep exposure: all fields are private and immutable
    // This datatype is NOT threadsafe, please access only from one thread
    
    public BoardTile(int i, int j, boolean hasBomb) {
        this.hasBomb = hasBomb;
        this.state = State.UNTOUCHED;
        this.i = i;
        this.j = j;
    }
    
    public int getI() {
        return i;
    }
    
    public int getJ() {
        return j;
    }
    
    public State getState() {
        return state;
    }
    
    public void setState(State state) {
        this.state = state;
        assert checkRep();
    }
    
    public boolean hasBomb() {
        return hasBomb;
    }
    
    public void setBomb(boolean hasBomb) {
        this.hasBomb = hasBomb;
        assert checkRep();
    }
    
    /**
     * Dig this tile and return true in case the tile had a bomb
     * If the state is either FLAGGED or DUG then do nothing and return false
     * @return
     */
    public boolean dig() {
        if (state == State.FLAGGED || state == State.DUG) {
            return false;
        }
        boolean exploded = hasBomb;
        hasBomb = false;
        state = State.DUG;
        checkRep();
        return exploded;
    }
    
    private boolean checkRep() {
        if (hasBomb) {
            return state != state.DUG;
        }
        return true;
    }
}
