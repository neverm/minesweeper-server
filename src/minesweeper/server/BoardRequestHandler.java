package minesweeper.server;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import minesweeper.Board;

/**
 * Interface to a minesweeper board that reads request messages, interacts with the board and replies with reply messages
 * @author nvm
 *
 */
public class BoardRequestHandler implements Runnable {
    
    private final Board board;
    private final BlockingQueue<RequestMessage> requests;
    private final Map<Integer, BlockingQueue<ResponseMessage>> subscribers;
   
    
    public BoardRequestHandler(Board board) {
        this.board = board;
        this.requests = new LinkedBlockingQueue<>();
        this.subscribers = new HashMap<>();
    }
    
    public BlockingQueue<RequestMessage> getRequestsQueue() {
        return requests;
    }
    
    @Override
    public void run() {
        while (true) {
            try {
                RequestMessage msg = requests.take();
                handleRequestMessage(msg);
            }
            catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        // infinite loop:
        // read next request message form queue (blocks)
    
        
    }
    
    public void subscribe(int key, BlockingQueue<ResponseMessage> subscriber) {
        subscribers.put(key, subscriber);
    }
    
    public void unsubscribe(int key) {
        
    }
    
    /**
     * Read the message, react and reply to it appropriately:
     * - for LOOK message reply with current state of the board
     * - for DIG, FLAG and DEFLAG messages mutate the board and
     * reply with current state of the board
     * - for DISCONNECT message do not reply and delete subscriber
     * Recipient of the reply message is specified in the request message
     * @param msg
     */
    private void handleRequestMessage(RequestMessage request) {
        ResponseMessage response;
        switch (request.getType()) {
            case DISCONNECT:
                // remove subscriber
                return;
            case LOOK:
                response = ResponseMessage.makeBoardMessage(board.toString());
                break;
            case DIG:
                boolean boom = board.dig(request.getI(), request.getJ());
                if (!boom) {
                    response = ResponseMessage.makeBoardMessage(board.toString());
                }
                else {
                    response = ResponseMessage.BOOM_MESSAGE;
                }
                break;
            case FLAG:
                board.flag(request.getI(), request.getJ());
                response = ResponseMessage.makeBoardMessage(board.toString());
                break;
            case DEFLAG:
                board.deflag(request.getI(), request.getJ());
                response = ResponseMessage.makeBoardMessage(board.toString());
                break;
            default:
                throw new UnsupportedOperationException("You should never get here");
        }
        reply(request.getSender(), response);
    }
    
    /**
     * Put given response message to the appropriate subscriber queue
     * assume that there is a queue for given receiverId
     * @param receiverId
     * @param response
     */
    private void reply(int receiverId, ResponseMessage response) {
        BlockingQueue<ResponseMessage> subscriber = subscribers.get(receiverId);
        try {
            subscriber.put(response);
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
