package minesweeper.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Client handler that handles a single client connection and facilitates
 * message passing between client and server
 * The methods in this class are supposed to be run in a separate thread on a server
 * Multiple handlers talk to the server object by posting messages in requestQueue and
 * get getting responses from responseQueue
 * @author nvm
 *
 */
public class ClientHandler implements Runnable {
    
    // Abstraction function: represent a client connection to the server with the client connection
    // being a pair of input and output streams and connection to the server a pair of blocking queues
    // rep invariant: client socket, request and response queues are not null
    // rep exposure: all fields are private and final
    // thread safety: this datatype is not threadsafe and should be interacted 
    // implicitly through responseQueue

    private final InputStream is;
    private final OutputStream os;
    private final MinesweeperServer server;
    private final BlockingQueue<RequestMessage> requestQueue;
    private final BlockingQueue<ResponseMessage> responseQueue;
    
    public ClientHandler(InputStream is, OutputStream os,
            BlockingQueue<RequestMessage> requestQueue, MinesweeperServer server) {
        this.is = is;
        this.os = os;
        this.server = server;
        this.requestQueue = requestQueue;
        this.responseQueue = new LinkedBlockingQueue<>();
    }
    
    public BlockingQueue<ResponseMessage> getResponseQueue() {
        return responseQueue;
    }
    
    /**
     * get integer that uniquely represent this client handler instance
     * Request messages from this client must be signed with this key
     * @return
     */
    public int getKey() {
        return hashCode();
    }
    
    @Override
    public void run() {
        handleClient();
    }
       
    /**
     * Handle single client connection by reading client messages from the input stream
     * and writing server replies to the output stream
     * Blocks when there are no messages from the client but the connection is still active
     */
    public void handleClient() {
        try (
            BufferedReader in = new BufferedReader(new InputStreamReader(is));
            PrintWriter out = new PrintWriter(os, true);
        ) {
            String helloMsg = 
                    String.format("Welcome to Minesweeper. Players: %d including you. "
                            + "Board: %d columns by %d rows. Type 'help' for help.",
                            server.getNumberOfClients(), server.getBoardWidth(), server.getBoardHeight());
            out.println(helloMsg);
            for (String line = in.readLine(); line != null; line = in.readLine()) {
                try {
                    boolean disconnect = handleRequest(line, out);
                    if (disconnect) {
                        in.close();
                        server.decrementNumClients();
                        return;
                    }
                }
                catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }
    
    
    /**
     * Handler for client input, that parses client input and reacts appropriately:
     * - for look message print to the client current board state
     * - for dig, flag or deflag messages mutate board and print the board state after
     * unless client has dug a mine. In that case print "boom!" to the client and return true (should disconnect)
     * - for bye message do not print anything and return true (should disconnect)
     * - for help, or message that doesn't match anything print help message
     * @param input message from client
     * @param client to write response to
     * @return true if the client should be disconnected
     */
    public boolean handleRequest(String input, PrintWriter client) throws InterruptedException {
        
        RequestMessage req;
        String helpMsg = "RTFM!";
        String reply;
        boolean shouldDisconnect = false;
        String regex = "(look)|(help)|(bye)|"
                     + "(dig -?\\d+ -?\\d+)|(flag -?\\d+ -?\\d+)|(deflag -?\\d+ -?\\d+)";
        if ( ! input.matches(regex)) {
            client.println(helpMsg);
            return false;
        }
        String[] tokens = input.split(" ");
        if (tokens[0].equals("look")) {
            req = new RequestMessage(RequestMessage.Type.LOOK, getKey());
        } else if (tokens[0].equals("help")) {
            client.println(helpMsg);
            return false;
        } else if (tokens[0].equals("bye")) {
            return true;
        } else {
            int x = Integer.parseInt(tokens[1]);
            int y = Integer.parseInt(tokens[2]);
            if (tokens[0].equals("dig")) {
                req = new RequestMessage(RequestMessage.Type.DIG, getKey(), y, x);
            } else if (tokens[0].equals("flag")) {
                req = new RequestMessage(RequestMessage.Type.FLAG, getKey(), y, x);
            } else if (tokens[0].equals("deflag")) {
                req = new RequestMessage(RequestMessage.Type.DEFLAG, getKey(), y, x);
            }
            else {
                // won't ever get here provided regex is correct, but the compiler doesn't know that
                client.println(helpMsg);
                return false;
            }
        }

        requestQueue.put(req);
        // read next board response from the response queue (blocking call)
        ResponseMessage response = responseQueue.take();
        // construct client reply string from board response message
        switch (response.getType()) {
            case BOOM:
                reply = "BOOM!";
                // only disconnect when we are not in debug mode
                shouldDisconnect = !server.isInDebugMode();
                break;
            case BOARD:
                reply = response.getBoard();
                break;
            default:
                throw new RuntimeException("Unsupported response message type " + response.getType());
        }
        
        client.println(reply);
        
        return shouldDisconnect;
    }
    
    
}
