package minesweeper.server;

import java.util.Optional;

// Immutable request message to a board request handler
public class RequestMessage {
    // Abstraction function:
    // represent a message of certain type to board request handler,
    // possibly having two arguments to specify target tile
    // and sender id that identifies the sender of the message
    // Rep. invariant:
    // - messages of type LOOK or DISCONNECT must not have arguments, so
    //   i and j are empty
    // - messages of types LOOK, DIG, FLAG, DEFLAG must have both arguments
    // set to some values
    // Rep. exposure: both fields are private final and no mutators are present
    
    public enum Type {LOOK, DIG, FLAG, DEFLAG, DISCONNECT}
    
    private final Type type;
    private final int sender;
    private final Optional<Integer> i, j;
    
    public RequestMessage(Type type, int sender) {
        this.type = type;
        this.j = this.i = Optional.empty();
        this.sender = sender;
        assert checkRep();
    }
    
    public RequestMessage(Type type, int sender, int i, int j) {
        this.type = type;
        this.i = Optional.of(i);
        this.j = Optional.of(j);
        this.sender = sender;
        assert checkRep();
    }
    
    public Type getType() {
        return type;
    }
    
    public int getI() {
        if (type == Type.LOOK || type == Type.DISCONNECT) {
            throw new IllegalStateException();
        }
        return i.get();
    }
    
    public int getJ() {
        if (type == Type.LOOK || type == Type.DISCONNECT) {
            throw new IllegalStateException();
        }
        return j.get();
    }
    
    public int getSender() {
        return sender;
    }
    
    private boolean checkRep() {
        boolean bothPresent = i.isPresent() && j.isPresent();
        if (type == Type.LOOK || type == Type.DISCONNECT) {
            return !bothPresent;
        }
        return bothPresent;
    }
}
