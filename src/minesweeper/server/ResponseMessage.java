package minesweeper.server;

import java.util.Optional;

//Immutable response message from a board request handler
public class ResponseMessage {
    // Abstraction function:
    // represent a message of certain type from a board request handler,
    // possibly having a board argument
    // Rep. invariant: if message is of type BOARD then the board field is not empty
    // Rep. exposure: all fields are private and final and no mutators are present
    public enum Type {BOARD, BOOM}
    
    public static final ResponseMessage BOOM_MESSAGE = new ResponseMessage(Type.BOOM);
    
    private final Type type;
    private Optional<String> board;
    
    public static ResponseMessage makeBoardMessage(String board) {
        return new ResponseMessage(Type.BOARD, board);
    }
    
    public Type getType() {
        return type;
    }
    
    /**
     * Get board argument
     * assume that message type is BOARD message
     * @return
     */
    public String getBoard() {
        return board.get();
    }
    
    private ResponseMessage(Type type) {
        this.type = type;
        this.board = Optional.empty();
        checkRep();
    }
    
    private ResponseMessage(Type type, String board) {
        this.type = type;
        this.board = Optional.of(board);
        checkRep();
    }
    
    private boolean checkRep() {
        if (type == Type.BOARD) {
            return board.isPresent() && board.get() != null;
        }
        else {
            return !board.isPresent();
        }
    }
    
    
}
