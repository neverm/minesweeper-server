# minesweeper-server

### Description
Server implementation of a simple protocol for classic minesweeper multiplayer game. This is the final assignment for MITx's 6.005.2x online course.
The game is started after the server program is run, and clients can send their requests according to the protocol in a real-time fashion.

### Protocol
The protocol is text based and consists of messages that server exchange with clients

#### Server messages

Formal grammar:

    MESSAGE ::= ( LOOK | DIG | FLAG | DEFLAG | HELP_REQ | BYE ) NEWLINE
    LOOK ::= "look"
    DIG ::= "dig" SPACE X SPACE Y
    FLAG ::= "flag" SPACE X SPACE Y
    DEFLAG ::= "deflag" SPACE X SPACE Y
    HELP_REQ ::= "help"
    BYE ::= "bye"
    NEWLINE ::= "\n" | "\r" "\n"?
    X ::= INT
    Y ::= INT
    SPACE ::= " "
    INT ::= "-"? [0-9]+

##### LOOK message

The message type is the word “look” and there are no arguments.

Returns a BOARD message, a string representation of the board’s state. Does not mutate anything on the server. See the section below on messages from the server to the user for the exact required format of the BOARD message.

##### DIG message

The message is the word “dig” followed by two arguments, the X and Y coordinates. The type and the two arguments are separated by a single SPACE.

Example:

    dig 3 10\n

The dig message has the following properties:

* If either x or y is less than 0, or either x or y is equal to or greater than the board size, or square x,y is not in the untouched state, do nothing and return a BOARD message.
* If square x,y’s state is untouched, change square x,y’s state to dug.
* If square x,y contains a bomb, change it so that it contains no bomb and send a BOOM message to the user. Then, if the debug flag is missing (see Question 4), terminate the user’s connection. See again the section below for the exact required format of the BOOM message. Note: When modifying a square from containing a bomb to no longer containing a bomb, make sure that subsequent BOARD messages show updated bomb counts in the adjacent squares. After removing the bomb continue to the next step.
* If the square x,y has no neighbor squares with bombs, then for each of x,y’s untouched neighbor squares, change said square to dug and repeat this step (not the entire DIG procedure) recursively for said neighbor square unless said neighbor square was already dug before said change.
* For any DIG message where a BOOM message is not returned, return a BOARD message.

##### FLAG message

The message type is the word “flag” followed by two arguments the X and Y coordinates. The type and the two arguments are separated by a single SPACE.

Example:

    flag 11 8\n

The flag message has the following properties:

* If x and y are both greater than or equal to 0, and less than the board size, and square x,y is in the untouched state, change it to be in the flagged state.
Otherwise, do not mutate any state on the server.
* For any FLAG message, return a BOARD message.

##### DEFLAG message

The message type is the word “deflag” followed by two arguments the X and Y coordinates. The type and the two arguments are separated by a single SPACE.

Example:

    deflag 9 9\n

The flag message has the following properties:

* If x and y are both greater than or equal to 0, and less than the board size, and square x,y is in the flagged state, change it to be in the untouched state.
Otherwise, do not mutate any state on the server.
* For any DEFLAG message, return a BOARD message.

##### HELP_REQ message

The message type is the word “help” and there are no arguments.
Returns a HELP message. Does not mutate anything on the server.

##### BYE message

The message type is the word “bye” and there are no arguments. Terminates the connection with this client.


#### Client messages

Formal grammar:

    MESSAGE ::= BOARD | BOOM | HELP | HELLO
    BOARD ::= LINE+
    LINE ::= (SQUARE SPACE)* SQUARE NEWLINE
    SQUARE ::= "-" | "F" | COUNT | SPACE
    SPACE ::= " "
    NEWLINE ::= "\n" | "\r" "\n"?
    COUNT ::= [1-8]
    BOOM ::= "BOOM!" NEWLINE
    HELP ::= [^\r\n]+ NEWLINE
    HELLO ::= "Welcome to Minesweeper. Players: " N " including you. Board: "
              X " columns by " Y " rows. Type 'help' for help." NEWLINE
    N ::= INT
    X ::= INT
    Y ::= INT
    INT ::= "-"? [0-9]+

There are no message types in the messages sent by the server to the user. The server sends the HELLO message as soon as it establishes a connection to the user. After that, for any message it receives that matches the user-to-server message format, other than a BYE message, the server should always return either a BOARD message, a BOOM message, or a HELP message.
For any message from the user which does not match the user-to-server message format, discard the incoming message and send a HELP message as the reply to the user.
The action to take for each different kind of message is as follows: 

##### HELLO message
This message should be sent to the user exactly as defined and only once, immediately after the server connects to the user. Again the message should end with a NEWLINE.
It informs user about current state of the game server: the size of the game board and the number of clients currently playing.

The hellow message itself:

    Welcome to Minesweeper. Players: N including you. Board: X columns by Y rows. Type 'help' for help.
    
##### BOARD message

The message has no start type word and consists of a series of newline-separated rows of space-separated characters, thereby giving a grid representation of the board’s state with exactly one char for each square. The mapping of characters is as follows:

* "-" for squares with state untouched.
* "F" for squares with state flagged.
* " " (space) for squares with state dug and 0 neighbors that have a bomb.
integer COUNT in range [1-8] for squares with state dug and COUNT neighbors that have a bomb.

In the printed BOARD output, the (x,y) coordinates start at (0,0) in the top-left corner, extend horizontally to the right in the X direction, and vertically downwards in the Y direction. (While different from the standard geometric convention for IV quadrant, this happens to be the protocol specification.)

##### BOOM message

The message is the word BOOM! followed by a NEWLINE

##### HELP message

The message is a sequence of non-NEWLINE characters followed by NEWLINE. The contents of HELP message is not specified by the protocol

