package minesweeper;

import static org.junit.Assert.*;

import org.junit.Test;

public class BoardTileTest {
    
    // Testing strategy:
    // test all valid combinations of:
    // - state
    // - having a bomb
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    @Test
    public void digDug() {
        BoardTile t = new BoardTile(0, 0, false);
        t.setState(BoardTile.State.DUG);
        boolean exploded = t.dig();
        assertEquals(BoardTile.State.DUG, t.getState());
        assertFalse(exploded);
    }
    
    @Test
    public void digFlaggedNoBomb() {
        BoardTile t = new BoardTile(0, 0, false);
        t.setState(BoardTile.State.FLAGGED);
        boolean exploded = t.dig();
        assertEquals(BoardTile.State.FLAGGED, t.getState());
        assertFalse(exploded);
    }
    
    @Test
    public void digFlaggedWithBomb() {
        BoardTile t = new BoardTile(0, 0, true);
        t.setState(BoardTile.State.FLAGGED);
        boolean exploded = t.dig();
        assertFalse(exploded);
    }
    
    
    @Test
    public void digUntouchedNoBomb() {
        BoardTile t = new BoardTile(0, 0, false);
        boolean exploded = t.dig();
        assertEquals(BoardTile.State.DUG, t.getState());
        assertFalse(exploded);
    }
    
    @Test
    public void digUntouchedWithBomb() {
        BoardTile t = new BoardTile(0, 0, true);
        boolean exploded = t.dig();
        assertEquals(BoardTile.State.DUG, t.getState());
        assertTrue(exploded);
    }
}
