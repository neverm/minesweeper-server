/* Copyright (c) 2007-2017 MIT 6.005 course staff, all rights reserved.
 * Redistribution of original or derived work requires permission of course staff.
 */
package minesweeper;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BoardTest {
    
    // Testing strategy
    // Test all valid tiles states from cross product
    // of tile type and the presence of bomb in that tile
    // Additionally, test digging untouched tile that doesn't have a bomb and:
    // - has one neighbor with a bomb
    // - has more than one neighbor with a bomb
    // - doesn't have neighbors with bombs
    
    private Board board;
    /**
     * The minefield should look like the following:
     * xoooo
     * xxooo
     * xxxoo
     * xxooo
     * xoooo
     */
    private boolean[][] minefield;
    
    @Before
    public void setUp() {
        minefield = new boolean[][] {
            {true, false, false, false, false},
            {true, true, false, false, false},
            {true, true, true, false, false},
            {true, true, false, false, false},
            {true, false, false, false, false}
        };
        board = new Board(minefield);
    }
    
    @Test(expected=AssertionError.class)
    public void testAssertionsEnabled() {
        assert false; // make sure assertions are enabled with VM argument: -ea
    }
    
    @Test
    public void checkInitial() {
        String boardStr = "- - - - -\n- - - - -\n- - - - -\n- - - - -\n- - - - -";
        assertEquals(boardStr, board.toString());
    }
    
    @Test
    public void flagMined() {
        String boardStr = "F - - - -\n- - - - -\n- - - - -\n- - - - -\n- - - - -";
        board.flag(0, 0);
        assertEquals(boardStr, board.toString());
    }
    
    @Test
    public void flagFlagged() {
        String boardStr = "F - - - -\n- - - - -\n- - - - -\n- - - - -\n- - - - -";
        board.flag(0, 0);
        board.flag(0, 0);
        assertEquals(boardStr, board.toString());
    }
    
    @Test
    public void flagEmpty() {
        String boardStr = "- F - - -\n- - - - -\n- - - - -\n- - - - -\n- - - - -";
        board.flag(0, 1);
        assertEquals(boardStr, board.toString());
    }
    
    @Test
    public void flagDug() {
        String boardStr = "- 3 - - -\n- - - - -\n- - - - -\n- - - - -\n- - - - -";
        boolean exploded = board.dig(0, 1);
        board.flag(0, 1);
        assertEquals(boardStr, board.toString());
    }
    
    @Test
    public void deflagFlagged() {
        String boardStr = "- - - - -\n- - - - -\n- - - - -\n- - - - -\n- - - - -";
        board.flag(0, 0);
        board.deflag(0, 0);
        assertEquals(boardStr, board.toString());
    }
    
    @Test
    public void deflagEmpty() {
        String boardStr = "- - - - -\n- - - - -\n- - - - -\n- - - - -\n- - - - -";
        board.deflag(0, 0);
        assertEquals(boardStr, board.toString());
    }
    
    @Test
    public void digMined() {
        String boardStr = "2 - - - -\n- - - - -\n- - - - -\n- - - - -\n- - - - -";
        boolean exploded = board.dig(0, 0);
        assertEquals(boardStr, board.toString());
        assertTrue(exploded);
    }
    
    @Test
    public void digMinedTwice() {
        String boardStr = "2 - - - -\n- - - - -\n- - - - -\n- - - - -\n- - - - -";
        boolean exploded1 = board.dig(0, 0);
        boolean exploded2 = board.dig(0, 0);
        assertTrue(exploded1);
        assertFalse(exploded2);
        assertEquals(boardStr, board.toString());
    }
    
    @Test
    public void digFlagged() {
        String boardStr = "F - - - -\n- - - - -\n- - - - -\n- - - - -\n- - - - -";
        board.flag(0, 0);
        boolean exploded = board.dig(0, 0);
        assertFalse(exploded);
        assertEquals(boardStr, board.toString());
    }
    
    @Test
    public void digWithMinedNeighbors() {
        String boardStr = "- 3 - - -\n- - - - -\n- - - - -\n- - - - -\n- - - - -";
        boolean exploded = board.dig(0, 1);
        assertFalse(exploded);
        assertEquals(boardStr, board.toString());
    }
    
    @Test
    public void digNoMinedNeighbors() {
        String boardStr = 
                  "- - 1    \n"
                + "- - 3 1  \n"
                + "- - - 1  \n"
                + "- - 3 1  \n"
                + "- - 1    ";
        boolean exploded = board.dig(0, 4);
        assertEquals(boardStr, board.toString());
        assertFalse(exploded);
    }
    
    @Test
    public void explosionRevealsNeighbors() {
        Board twoMines = new Board(new boolean[][]
                { {true, false, false},
                  {false, false, false},
                  {false, false, true}});
        String expected =
                  "     \n"
                + "  1 1\n"
                + "  1 -";
        twoMines.dig(0, 0);
        assertEquals(expected, twoMines.toString());
        
    }
    
    @Test
    public void revealStopsWhenNeighborsWithMines() {
        Board tall = new Board(new boolean[][] {
            {false, false},
            {false, false},
            {false, true},
            {true, false},
            {false, false},
            {false, false}
        });
        String expected = 
                  "   \n"
                + "1 1\n"
                + "- -\n"
                + "- -\n"
                + "- -\n"
                + "- -";
        tall.dig(0, 0);
        assertEquals(expected, tall.toString());
    }
    
    
    public void providedDigByDifferentUser() {
        Board tall = new Board(new boolean[][] {
            {false, false},
            {false, false},
            {false, true},
            {true, false},
            {false, false},
            {false, false},
            {false, false},
            {false, false},
            {false, false},
            {true, false},
            {false, true},
            {false, false},
            {false, false},
            {false, false},
            {false, false},
            {false, false},
            {false, true},
            {true, false},
            {false, false},
            {false, false}

        });
        String expected = 
                  "   \n"
                + "1 1\n"
                + "- -\n"
                + "- 2\n"
                + "1 1\n"
                + "   \n"
                + "   \n"
                + "   \n"
                + "1 1\n"
                + "- -\n"
                + "- -\n"
                + "- -\n"
                + "- -\n"
                + "- -\n"
                + "- -\n"
                + "- -\n"
                + "- -\n"
                + "- -\n"
                + "- 1\n"
                + "- -";
        tall.dig(0, 0);
        tall.dig(1, 3);
        tall.dig(1, 5);
        tall.dig(1, 7);
        tall.dig(1, 18);
        assertEquals(expected, tall.toString());
    }
}
